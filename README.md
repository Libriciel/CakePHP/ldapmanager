
Exemple Configure 

Configure::write('LdapManager.Ldap.use', true);
Configure::write('LdapManager.Ldap.type', 'OpenLDAP');
Configure::write('LdapManager.Ldap.host', 'x.x.x.x');
Configure::write('LdapManager.Ldap.host_fall_over', 'x.x.x.x');
Configure::write('LdapManager.Ldap.port', '389');
Configure::write('LdapManager.Ldap.basedn', 'DC=adullact,DC=win');
Configure::write('LdapManager.Ldap.login', 'cn=ldapreader,OU=Informatique,DC=adullact,DC=win');
Configure::write('LdapManager.Ldap.password', 'ldapreader');
Configure::write('LdapManager.Ldap.tls', false);
Configure::write('LdapManager.Ldap.version', 3);
Configure::write('LdapManager.Ldap.account_suffix', '');
Configure::write('LdapManager.Ldap.fields', array (
  'User' => 
  array (
    'username' => 'samaccountname',
    'note' => 'info',
    'nom' => 'sn',
    'prenom' => 'givenname',
    'email' => 'mail',
    'telfixe' => 'telephonenumber',
    'telmobile' => 'mobile',
    'active' => '',
  ),
));
Configure::write('LdapManager.Ldap.certificat', '');
Configure::write('LdapManager.Ldap.forceLogin', false);
Configure::write('LdapManager.Ldap.filter', '');
