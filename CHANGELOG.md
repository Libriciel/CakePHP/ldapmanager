# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.

Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/) et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [1.1.6] - 2022-09-05

### Correction
- Connexion ldap impossible

## [1.1.5] - 2022-07-22

### Correction
- Correction d'une clé de tableau pour le filtre

## [1.1.4] - 2022-05-06

### Ajout
- Mise en place d'un filtre dans la configuration du LDAP pour améliorer la recherche

## [1.1.3] - 2022-05-03

### Corrections
- Prise en compte du port de configuration
- Prise en charge d'un filtre de connexion pour la recherche des utilisateurs 

## [1.1.2] - 2022-01-20

### Corrections
- Prise en charge d'un uid différent de samaccountname pour les serveurs active directory 

## [1.1.1] - 2019-10-19

### Ajout
- Option pour forcer l'authentification même si l'utilisateur n'existe pas dans le LDAP

## [1.1.0] - 2020-09-17

### Ajout
- Prise en compte d'un certificat pour la connexion a un LDAPS

## [1.0.4] - 2019-02-06

### Correction
- Génération du schéma de base pour validation

## [1.0.3] - 09/04/2018

### Corrections
- Problème de boucle d'appel à la sauvegarde d'un user depuis le LDAP #1

## [1.0.2]

### Corrections
- [View] Select2

## [1.0.1]

### Corrections
- [Database] Suppression des transactions des script sql
- [Database] Prise en compte de DN plus long que 254 caractères
- [Database] Prise en compte de NAME jusqu'à 500 caractères

## [1.0.0]

### Ajouts
- Mise en production

## [0.9.0]

### Ajouts
- Création du plugin CakePHP LdapManager

[1.0.4]: https://gitlab.libriciel.fr/CakePHP/LdapManager/compare/1.0.3...1.0.4
[1.0.3]: https://gitlab.libriciel.fr/CakePHP/LdapManager/compare/1.0.2...1.0.3
[1.0.2]: https://gitlab.libriciel.fr/CakePHP/LdapManager/compare/1.0.1...1.0.2
[1.0.1]: https://gitlab.libriciel.fr/CakePHP/LdapManager/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.libriciel.fr/CakePHP/LdapManager/compare/0.9.0...1.0.0
[0.9.0]: https://gitlab.libriciel.fr/CakePHP/LdapManager/tags/0.9.0
