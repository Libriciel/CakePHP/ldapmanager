<?php
/**
 * Acl Manager
 *
 * A CakePHP Plugin to manage Acl
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Frédéric Massart - FMCorz.net
 * @copyright     Copyright 2011, Frédéric Massart
 * @link          http://github.com/FMCorz/AclManager
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('ConnectionManager', 'Model');

if( !defined( 'LDAP_MANAGER_DIR' ) ) {
        define( 'LDAP_MANAGER_DIR', dirname( __FILE__ ).DS.'..'.DS );
}

/**
 * END OF USER SETTINGS
 */

/*Configure::write("AclManager.version", "1.2.5");
if (!is_array(Configure::read('AclManager.aros'))) {
	Configure::write('AclManager.aros', array(Configure::read('AclManager.aros')));
}
if (!is_array(Configure::read('AclManager.ignoreActions'))) {
	Configure::write('AclManager.ignoreActions', array(Configure::read('AclManager.ignoreActions')));
}
if (!Configure::read('AclManager.models')) {
	Configure::write('AclManager.models', Configure::read('AclManager.aros'));
}*/

$versionFile = file( LDAP_MANAGER_DIR . DS . 'VERSION.txt');
Configure::write("LdapManager.version", trim(array_pop($versionFile)));
