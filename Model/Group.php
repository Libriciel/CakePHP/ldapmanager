<?php

class Group extends LdapManagerAppModel
{
    public $tablePrefix = 'ldapm_';
    
    public $actsAs = array('Tree');
    
    public $hasMany = array(
        'ModelGroup' => array(
            'className' => 'LdapManager.ModelGroup',
            'foreignKey' => 'ldapm_groups_id ',
            'dependent' => true
        )
    );
    
}
