<?php

class LdapTypeBehavior extends ModelBehavior {

    public $_mapLdapKeyWord = array();
    
    public $_ldapSource = array();
    
       
    public function setup(Model $model, $settings = array()) {
        if (empty($this->_mapLdapKeyWord)) {
            $this->_ldapSource=$model->getDataSource();

            $model->useTable ='';

            if(!empty($this->_ldapSource->config['type']))
            {
                switch ($this->_ldapSource->config['type']) {
                    case 'OpenLDAP':
                        $this->primaryKey = 'cn';
                        break;
                    case 'ActiveDirectory':
                        $this->primaryKey = 'dn';
                        $this->_setMappingsActiveDirectory();
                        break;
                    
                    case 'OracleInternetDirectory':
                        $this->primaryKey = 'dn';
                        break;
                    default:
                        break;
                }  
            }
        }
        
    }
    
    protected function _setMappingsActiveDirectory() {

        $this->_mapLdapKeyWord=array(
            'objectClass'=>'objectCategory',
            'groupOfNames'=>'group',
            'cn'=>'samaccountname',
            'inetOrgPerson'=>'user',
            'uid'=> $this->_ldapSource->config['fields']['User']['username']
        );
    }
    
    public function getKeyWord(Model &$model, $key)
    {
        if (!empty($this->_mapLdapKeyWord[$key])) {
            return $this->_mapLdapKeyWord[$key];
        }

        return $key;
    }
    
    public function getType(Model &$model)
    {
        return $this->_ldapSource->config['type'];
    }
    
    public function getFilter(Model &$model){
        
        return $this->_ldapSource->config['filter'];
    }
    
    public function getFieldsUser(Model &$model)
    {
        foreach($this->_ldapSource->config['fields']['User'] as $field)
        {
            if(!empty($field)) {
                $user_fields[]=$field;
            }
        }
        
        return $user_fields;
    }
    
    public function getFieldsUserActive(Model &$model)
    {
        return $this->_ldapSource->config['fields']['User']['active'];
    }
    
    public function getFieldsUserMap(Model &$model){
        
        return array_flip($this->_ldapSource->config['fields']['User']);
    }
}
