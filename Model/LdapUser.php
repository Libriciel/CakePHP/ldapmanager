<?php

class LdapUser extends LdapManagerAppModel
{
    
    public $name = 'User';
    public $useTable = false;
    public $useDbConfig = 'ldap';
    
    public $actsAs = array( 'LdapManager.LdapType');
    
    /**
     * return true if the userName is a member of the groupName
     * Active Directory group
     */
    function isAuthenticate($userName, $userPassword)
    {
        $this->useTable='';
        $ldap = $this->getDataSource();
        
        // trivial check for valid names
        if (empty($userName) || empty($userPassword)) {
            return false;
        }

        if (!empty($ldap->config['account_suffix'])) {
            $userName = $userName . $ldap->config['account_suffix'];
        }
        
        // locate the user record
        $userData = $this->find('first',
                                array(
                                    'fields' => array($ldap->config['fields']['User']['username']),
                                    'conditions' => array('AND' => array(
                                        $ldap->config['fields']['User']['username'] => $userName
                                            )
                                        ),
                                        'order' => $ldap->config['fields']['User']['username'],
                                        'recursive' => -1,
                                    )
                                );
        if (!empty($userData)) {
            return $ldap->auth($userData['User']['dn'], $userPassword);
        }
        if ($ldap->config['forceLogin']==true) {
            return $ldap->auth($userName, $userPassword);
        } 
                
        // no user by that name exists
        return false;
    }
    
    /** FIX
     * return true if the userName is a member of the groupName
     * Active Directory group
     */
    function isMemberOf($userName, $groupName)
    {
        $this->useTable='';
        $ldap = $this->getDataSource();
        
        // trivial check for valid names
        if (empty($userName) || empty($groupName)) {
            return false;
        }

        // locate the user record
        $userData = $this->find('all',
                                array(
                                    'fields'=> $ldap->config['fields']['User']['username'],
                                    'conditions' => array('AND'=>array(
                                        $ldap->config['fields']['User']['username'] => $userName
                                       // 'UserAccountControl:1.2.840.113556.1.4.803:' => '512'    
                                            )
                                        ),
                                        'order'=> $ldap->config['fields']['User']['username'],
                                        'recursive'=> -1,
                                    )
                                );
        
        // no user by that name exists
        if (empty($userData)) {
            return false;
        }

        // check if the userin question belongs to any groups
        if (!isset($userData['User']['memberof'])) {
            return false;
        }

        // search all groups that our user if a meber
        $groups = $userData['User']['memberof'];
        foreach ($groups as $group) {
            if (strpos($group, $groupName) != false) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * return true if the userName is a member of the groupName
     * Active Directory group
     */
    function getActifMemberOf($groupNameDn)
    {
        // trivial check for valid names
        if (empty($groupNameDn)) {
            return false;
        }

        $this->useTable='';
        // locate the user record
        $users = $this->find('first',
                                array(
                                    'fields'=> array($this->getKeyWord('member'), $this->getKeyWord('uniqueMember')),
                                    'targetDn'=> $groupNameDn,
                                    'recursive'=> -1,
                                    'conditions' => array(
                                                'OR' =>  array(
                                                    $this->getKeyWord('objectClass') => array(
                                                        $this->getKeyWord('groupOfNames'), 
                                                        $this->getKeyWord('groupOfUniqueNames'))
                                                    )),
                                    )
                                );
        
        if(empty($users)){
            return false;
        }
        
        $userData = array();
        $members = array();
        if(!empty($users['User'][$this->getKeyWord('member')])){
           $values = array($this->getKeyWord('member'), $this->getKeyWord('uniqueMember'));
           foreach ($values as $key => $value) {
                $liste_users = array();
                if(!empty($users['User'][$value]) && !is_array($users['User'][$value])){
                    $liste_users = array($users['User'][$value]);
                }elseif(!empty($users['User'][$value])) {
                    $liste_users = $users['User'][$value];
                }
                
                $members = array_merge($members, $liste_users);
           }
        }   

        if(!empty($members)){
            foreach($members as $member){
                $conditions = array();
                $active=$this->getFieldsUserActive();
                if(!empty($active)){
                    $conditions = array('AND' => array($active => '1'));
                }
                if ($this->getType() == 'ActiveDirectory') {//$this->getFieldsUserActive()
                    $conditions = array('AND' => array('UserAccountControl:1.2.840.113556.1.4.803:' => 512));
                }
                // locate the user record
                $userData[] = $this->find('first',
                                    array(
                                        'fields'=> array_values($this->getFieldsUser()),
                                        'targetDn'=> $member,
                                        'conditions' => $conditions,
                                            'recursive'=> -1,
                                        )
                                    );
            }
        }
        
        // no user by that name exists
        if (empty($userData)) {
            return false;
        }


        return $userData;
    }
    
}
