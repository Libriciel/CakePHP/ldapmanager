<?php

class UserTask extends Shell {
    
    public $uses = array('User','LdapManager.LdapUser', 'LdapManager.ModelGroup', 'LdapManager.Group');

    public function execute() {
    }
    
    public function update() {
        
        if (!method_exists($this->User, 'saveLdapManagerLdap')) {
            Shell::error(__d('cake_dev', 'Callback %s not defined in %s', 'saveLdapManagerLdap()', $this->User->alias));
        }
        
        $dataSource = $this->User->getDataSource();
        //$dataSource->useNestedTransactions = false;
        try
        {
            $dataSource->begin();
            $fields_ldap=$this->LdapUser->getFieldsUserMap();
            
            //Liste groups ldap
            $model_groups=$this->ModelGroup->find('all', array(
                'contain' => 'Group',
                'recursive'=>-1));
            
            foreach($model_groups as $group)
            {
                Shell::out(__('Recherche d\'utilisateurs dans le groupe: ').$group['Group']['name']);
                $usersLdap = $this->LdapUser->getActifMemberOf($group['Group'][$this->LdapUser->getKeyWord('dn')]);
                if(!empty($usersLdap)){
                    foreach($usersLdap as $user_ldap_key=>$user_ldap)
                    {
                        $this->User->recursive=-1;
                        $UserId=$this->User->{'findBy'.$fields_ldap[$this->LdapUser->getKeyWord('uid')]}( $user_ldap['User'][$this->LdapUser->getKeyWord('uid')], array('id') );  
                        if(empty($UserId)){
                            $this->User->create();
                            $data = array();
                            foreach ($fields_ldap as $key_field => $field) {
                                if (!empty($user_ldap['User'][$key_field])) {
                                    $data['User'][$field] = $user_ldap['User'][$key_field];
                                }
                            }
                            $data['User'][strtolower($group['ModelGroup']['model'].'_id')] = $group['ModelGroup']['foreign_key'];
                            $user_id=$this->User->saveLdapManagerLdap($data);
                            if(!$user_id){
                                Shell::err('création de l\'utilisateur echoué : '. $user_ldap['User'][$this->LdapUser->getKeyWord('uid')]);
                            }
                        }
                        $this->User->clear();
                    }
                }
                Shell::hr();
            }
            
            $dataSource->commit();
            return true;
        }
        catch (ErrorException $e)
        {
            $dataSource->rollback();
            Shell::error($e->getMessage());
        }
        
        return false;
    }
    
}
