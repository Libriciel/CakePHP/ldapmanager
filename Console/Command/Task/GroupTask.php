<?php

class GroupTask extends Shell {
    
    public $uses = array('LdapManager.LdapGroup', 'LdapManager.Group');

    public function execute() { }
    
    public function update() {
           
        $dataSource = $this->Group->getDataSource();
        $dataSource->useNestedTransactions = false;
        try
        {
            //Liste groups ldap
            $ldapGroups= $this->LdapGroup->find('all',
                                    array(
                                        'fields' => array($this->LdapGroup->getKeyWord('cn')),
                                        'conditions' => array(
                                                'OR' =>  array(
                                                    $this->LdapGroup->getKeyWord('objectClass') => array(
                                                        $this->LdapGroup->getKeyWord('groupOfNames'), 
                                                        $this->LdapGroup->getKeyWord('groupOfUniqueNames'))
                                                    )),
                                        'targetDn' => $this->LdapGroup->getFilter(),
                                        'recursive' => -1,
                                        'order'=> $this->LdapGroup->getKeyWord('cn')
                                    )
                                );
            $dataSource->begin();
            
            if(!empty($ldapGroups))
            foreach($ldapGroups as $ldapGroup)
            {
                $this->Group->recursive=-1;
                $GroupId=$this->Group->findByName($ldapGroup['Group'][$this->LdapGroup->getKeyWord('cn')], array('id'));   
                if(empty($GroupId)){
                    $this->Group->create();
                    $data = array();
                    $data['Group']['name'] = $ldapGroup['Group'][$this->LdapGroup->getKeyWord('cn')];
                    $data['Group']['dn'] = $ldapGroup['Group']['dn'];
                    $this->Group->save($data);
                }

            }
            
            $dataSource->commit();
            return true;
        }
        catch (ErrorException $e)
        {
            $dataSource->rollback();
            $this->out($e->getMessage());
        }
        
        return false;
    }
    
}
