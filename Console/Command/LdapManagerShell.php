<?php

/**
 * Shell for Group manager
 *
 * @package     LdapManager
 * @subpackage	LdapManager.Console.Command
 */
class LdapManagerShell extends Shell {

    public $tasks = array('LdapManager.Group','LdapManager.User');
    
    public function main() {
        $this->out('Script de patch de Webdelib');
        // Désactivation du cache
        Configure::write('Cache.disable', true);
        // Création de styles perso
        $this->stdout->styles('time', array('text' => 'magenta'));
        $this->stdout->styles('important', array('text' => 'red', 'bold' => true));
        
         // Solution alternative plus élégante mais où il faut correctement nommer les fonctions
        if (method_exists($this, $this->command))
            $this->runCommand($this->command, $this->args);
        else
            $this->out("erreur");
    }

    /**
     * Options d'éxecution et validation des arguments
     *
     * @return Parser $parser
     */
    public function getOptionParser()
    {
        $plugin = array(
			'short' => 'p',
			'help' => __('Plugin to process'),
			);
        return parent::getOptionParser()
                ->description(__("Better manage, and easily synchronize you application's Group"))
                ->addSubcommand('group_update', array(
                        'parser' => array(
                                'options' => compact('plugin'),
                                ),
                        'help' => __('Add new Groups. Does not remove element from the Group table.')
                ))->addSubcommand('group_sync', array(
                        'parser' => array(
                                'options' => compact('plugin'),
                                ),
                        'help' => __('Perform a full sync on the ACO table.' .
                                'Will create new ACOs or missing controllers and actions.' .
                                'Will also remove orphaned entries that no longer have a matching controller/action')
                ))->addSubcommand('user_update', array(
                        'help' => __('Add new Users. Does not remove element from the Users table.'),
                        'parser' => array(
                                'options' => compact('plugin'),
                                ),
                ))->addSubcommand('user_sync', array(
                        'help' => __('Recover a corrupted Tree'),
                        'parser' => array(
                                'arguments' => array(
                                        'type' => array(
                                                'required' => true,
                                                'help' => __('The type of tree to recover'),
                                                'choices' => array('aco', 'aro')
                                        )
                                )
                        )
                ));
    }

/**
 * Sync the ACO table
 *
 * @return void
 **/
	public function group_sync() {
                return $this->Group->update();
	}

/**
 * Updates the Aco Tree with new controller actions.
 *
 * @return void
 **/
	public function group_update() {
		return $this->Group->update();
	}
        
/**
 * Updates the Aco Tree with new controller actions.
 *
 * @return void
 **/
	public function user_update() {
		return $this->User->update();
	}

/**
 * Verify a Acl Tree
 *
 * @param string $type The type of Acl Node to verify
 * @access public
 * @return void
 */
	public function verify() {
		/*$this->LdapManager->args = $this->args;
		return $this->LdapManager->verify();*/
	}
/**
 * Recover an Acl Tree
 *
 * @param string $type The Type of Acl Node to recover
 * @access public
 * @return void
 */
	public function recover() {
		/*$this->LdapManager->args = $this->args;
		$this->LdapManager->recover();*/
	}
}
